from gpiozero import LED
from gpiozero import Button
from time import sleep

led = LED(12)
button = Button(21)

# Toggle a light based on pressing a button

# Start with the LED off

# Wait for the button to be pressed.

# If the LED was on, turn it off. If the LED was off, turn it on. 

# Repeat until the program stops.


# code to solve problem goes here

led.off()
is_on = False

while True:
    if button.is_pressed == True:
        if is_on:
            led.off()
            is_on = False
            sleep(0.25)
        else:
            led.on()
            is_on = True
            sleep(0.25)
    