from gpiozero import LED
from time import sleep

led = LED(12)

# Countdown to zero

# Turn off the LED

# Blink the LED (¼ second on, ¾ second off)
# for 10 times then leave the LED on.


# code to solve problem goes here

led.off()  #start with LED off
sleep(1)

counter = 10

while counter > 0:
    led.on()
    sleep(0.25)
    led.off()
    sleep(0.75)
    
    counter = counter - 1

led.on()




