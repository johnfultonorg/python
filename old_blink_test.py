import time
import RPi.GPIO as GPIO


#pin definition
led_pin = 12
interval = 0.3

# warnings?
GPIO.setwarnings(False)


#use GPIO pin numbering
GPIO.setmode(GPIO.BCM)

#set led pin as output
GPIO.setup(led_pin, GPIO.OUT)

while True:
    GPIO.output(led_pin, GPIO.HIGH)
    time.sleep(interval)
    GPIO.output(led_pin, GPIO.LOW)
    time.sleep(interval)